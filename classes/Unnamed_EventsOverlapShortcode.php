<?php
namespace UnnamedClasses;

use DateTime;

class Unnamed_EventsOverlapShortcode
{

  function __construct()
  {
    add_shortcode( 'stringToEvents', [$this, 'stringToEvents'] );
  }

  public static function stringToEvents($atts, $content = null){
    $atts = shortcode_atts(
  		[],
  		$atts
  	);

    if (empty($content)) {
      $content = "1630 1700
      1600 1615
      1000 1730
      1530 2100";
    }

    $times=$this->convertToArray($content);

    $overlaps=$this->checkOverlap($times);
    if (count($overlaps)>0) {
      $html='';
      foreach ($overlaps as $key => $val) {
        $html.=sprintf('<li>%s</li>', $val);
      }

      return sprintf('<ul>%s</ul>', $html);
    }
  }

  private function checkOverlap($events){
    $results=[];

    foreach ($events as $thisevent) {
      $conflicts = 0;
      foreach ($events as $thatevent) {
        if ($thisevent[0]==$thatevent[0]) {
          continue;
        }

        $diff_start=$thisevent[1]->diff($thatevent[2], true);
        $diff_end=$thisevent[2]->diff($thatevent[1], true);

        if ($thisevent[1]<$thatevent[2]&&$thisevent[2]>$thatevent[1]) {
          $conflicts++;
          $results[] = "Event #" . $thisevent[0] . " overlaps with Event # " . $thatevent[0] . " \n";
        }
      }
    }

    return $results;
  }

  private function convertToArray($data){
    $arr=explode(PHP_EOL, $data);

    $times=[];

    foreach ($arr as $key => $elem) {
      $timesArr=[$key+1];
      $startEnd=explode(' ', trim($elem));

      foreach ($startEnd as $key=>$val) {
        $time=implode(':', str_split(trim($val), 2)).':00';
        $timesArr[]=new DateTime($time);
      }

      $times[]=$timesArr;
    }

    return $times;
  }
}
