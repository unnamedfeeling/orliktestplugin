<?php
namespace UnnamedClasses;

class Unnamed_FilterArr
{
  public $prefix='advanced_options_';

  function __construct()
  {
    add_shortcode( 'filter_arr', [$this, 'filter_arr'] );
    add_shortcode( 'filter_arr_alt', [$this, 'filter_arr_alt'] );
  }

  // task 1: solution 1
  public static function filter_arr(){
    $arr=array(1,1,1,2,2,2,2,3);
    $out=[];

    foreach ($arr as $key => $val) {
      if(!in_array($val, $out)){
        $out[]=$val;
      }
    }

    $result=sprintf(
      '<pre>%s</pre>',
      var_export($out, true)
    );

    return $result;
  }

  // task 1: solution 2
  public static function filter_arr_alt(){
    $arr=array(1,1,1,2,2,2,2,3);

    $out=array_values(array_unique($arr, SORT_NUMERIC));

    $result=sprintf(
      '<pre>%s</pre>',
      var_export($out, true)
    );

    return $result;
  }
}
