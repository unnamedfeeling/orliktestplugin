<?php
namespace UnnamedClasses;

use WP_Query;

class Unnamed_EventsShortcode
{
  public $prefix='advanced_options_';

  function __construct()
  {
    add_shortcode( 'EVENTS', [$this, 'events_sc'] );
  }

  public static function events_sc( $atts ) {
  	$atts = shortcode_atts(
  		[
  			'amount' => '8',
  		],
  		$atts
  	);

    return $this->returnEventsHtml($atts);
  }

  private function returnEventsHtml($atts){
    $html='';

    $evts_html='<ul>%s</ul>';

    $args=[
      'post_type' => 'events',
      'posts_per_page' => (int)$atts['amount']
    ];

    $evts=new WP_Query($args);
    if ($evts->have_posts()) {
      while ($evts->have_posts()) {
        $evts->the_post();

        $evmeta=get_post_meta( $evts->post->ID, '', false );

        $html.=sprintf(
          '<li>%s</li>',
          sprintf(
            '<a href="%s">%s%s</a>',
            get_the_permalink( $evts->post->ID ),
            $evts->post->post_title,
            (!empty($evmeta[$this->prefix.'start-time'][0])&&!empty($evmeta[$this->prefix.'end-time'][0]))?
            sprintf(' (%s - %s)', $evmeta[$this->prefix.'start-time'][0], $evmeta[$this->prefix.'end-time'][0]) : ''
          )
        );
      }
    }

    return sprintf($evts_html, $html);
  }

}
