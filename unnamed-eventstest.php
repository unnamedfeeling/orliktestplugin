<?php
use UnnamedClasses\Unnamed_Eventstest;

/**
 *
 * @link              https://t.me/unnamedfeeling777
 * @since             1.0.0
 * @package           Unnamed_Eventstest
 *
 * @wordpress-plugin
 * Plugin Name:       Events test plugin
 * Plugin URI:        https://t.me/unnamedfeeling777
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Alexandr Yarosh
 * Author URI:        https://t.me/unnamedfeeling777
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       unnamed-eventstest
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'UNNAMED_EVENTSTEST_VERSION', '1.0.0' );

require 'vendor/autoload.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_unnamed_eventstest() {

	$plugin = new Unnamed_Eventstest();
	$plugin->run();
}
run_unnamed_eventstest();
